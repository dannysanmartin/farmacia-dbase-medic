import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../app/modules/dashboard/dashboard.component';
import { LoginFarmaciaComponent } from '../app/modules/login/login-farmacia/login-farmacia.component';
import { HomeComponent } from './modules/dashboard/pages/home/home.component';


const routes: Routes = [
  /* {
     path: '',
     redirectTo: "/",
     pathMatch: "full",
    
   },
   *
  /* {path: 'loginFarmacia',component: LoginFarmaciaComponent},
   {path: 'dashboard',component: DashboardComponent},
   */
  {
    path: 'login',

    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
    //  canLoad: [ AuthGuardServiceGuard ]
  },

  {
    path: '',
    // canLoad: [ AuthGuardServiceGuard ],
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule),

  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
