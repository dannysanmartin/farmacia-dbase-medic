import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService as AuthServiceLocal } from '../../shared/services/auth.service';
import { Observable } from 'rxjs';
import { HeaderButtonsService } from '../../shared/services/header-buttons.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  header = 'inicio';
  data: any;
  constructor(
    private auth: AuthServiceLocal, 
    private router: Router,
  ) {
    this.data =  JSON.parse(localStorage.getItem('account'));
   }

  ngOnInit() {
  }
  
  cargarheader(nombre) {
    this.header = nombre;
  }

}
