import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { AuthService } from 'src/app/shared/services/auth.service';

export class Usuario {
  id: number;
  nombre: string;
  detalle: string;
  opciones: string;

}

@Component({
  selector: 'app-catalogos',
  templateUrl: './catalogos.component.html',
  styleUrls: ['./catalogos.component.scss']
})

export class CatalogosComponent implements OnInit {
  displayedColumns = ['nombre', 'detalle', 'opciones'];
  dataSource: any;
  name: any;
  detail: any;
  id: any;
  editar = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(
    private auth: AuthService
  ) { }



  ngOnInit() {
    this.getTipoCatalogos();
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getTipoCatalogos() {
    this.auth.getTipoCatalogos().subscribe(data => {
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  editarTipoCata(id, nombre, detalles) {
    this.name = nombre;
    this.detail = detalles;
    this.id = id;
    console.log(id, this.name, this.detail);

  }

  createTp(nombre, detalle) {
    let info = [];
    this.name = nombre;
    this.detail = detalle;
    var res = this.detail.split(",");
    info.push(res);
    const dataInf = {
      nombre: this.name,
      detalles: this.detail,
    }

    console.log('armadojson', dataInf)


    this.auth.createTipoCatalogo(dataInf).subscribe(data => {

      document.getElementById('closeModalTC').click();
      this.dataSource.data.push(data);
      console.log(' this.dataSource.data', this.dataSource.data)
    }, (err) => {
      console.log('error')
    });

    this.name = null;
    this.detail = null;
    this.refreshTable();;

  }


  private refreshTable() {
    this.auth.getTipoCatalogos().subscribe(data => {

        
    })
  }


  updateTp(nombre, detalle) {
    let info = [];
    this.name = nombre;
    this.detail = detalle;
    var res = this.detail.split(",");
    info.push(res);
    const dataInf = {
      nombre: this.name,
      detalles: info[0],
    }
    console.log('data', dataInf)
    this.auth.updateTipoCatalogo(dataInf, this.id).subscribe(data => {

      document.getElementById('closeModalTC').click();
      this.dataSource.data.push(data);
      console.log(' this.dataSource.data', this.dataSource.data)
    }, (err) => {
      console.log('error')
    });

    this.name = null;
    this.detail = null;
    this.id = null;

  }


}
export class FormFieldOverviewExample { }
