import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { InventarioComponent } from './pages/inventario/inventario.component';
import { CatalogosComponent } from './pages/catalogos/catalogos.component';



const routes: Routes = [
  {
    path: '', 
    component: DashboardComponent, children: [
      { path: '', redirectTo: 'inicio', pathMatch: 'full' },
      { path: 'inicio', component:  HomeComponent},
      { path: 'inventario', component:  InventarioComponent},
      { path: 'catalogos', component:  CatalogosComponent},
     
     
    ]
  },
  
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
