import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { InventarioComponent } from './pages/inventario/inventario.component';

import { MatDatepickerModule, MatInputModule, MatPaginatorModule, MatTableModule,
  MatSortModule, MatPaginatorIntl } from '@angular/material';
  
import { CatalogosComponent } from './pages/catalogos/catalogos.component';
import { PaginatorEspañol } from './paginator-español';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    InventarioComponent,
    CatalogosComponent,

  ], 
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDatepickerModule
    
  ],
  providers: [{ provide: MatPaginatorIntl, useClass: PaginatorEspañol}],
  
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
  
})
export class DashboardModule { }

