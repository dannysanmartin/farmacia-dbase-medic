import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginFarmaciaComponent } from './login-farmacia/login-farmacia.component';
import { MatDatepickerModule, MatNativeDateModule, MatInputModule, MatFormFieldModule, MatSnackBarModule } from '@angular/material';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

@NgModule({
  declarations: [
    LoginFarmaciaComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    ShowHidePasswordModule,
  ],
  exports: [   
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    FormsModule
  ],
})
export class LoginModule { }
