import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginFarmaciaComponent } from './login-farmacia.component';

describe('LoginFarmaciaComponent', () => {
  let component: LoginFarmaciaComponent;
  let fixture: ComponentFixture<LoginFarmaciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginFarmaciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFarmaciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
