import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login-farmacia',
  templateUrl: './login-farmacia.component.html',
  styleUrls: ['./login-farmacia.component.scss']
})
export class LoginFarmaciaComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private auth: AuthService, private router: Router) { }

  ngOnInit() {
    /*if (this.auth.isAuthenticated()) {
      console.log('ya esta logado');
      this.router.navigate(['/']);

    } else {
      this.registerForm = this.formBuilder.group({
        username: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
      });
     
    }*/

    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.login()
    
  }

  login() {
    this.auth.getTokenUser(this.registerForm.value).subscribe(data => {
      this.getIdFarm(data);
    }, (err) => {
      this.openSnackBar('Error legeando');
    });
  }
  getIdFarm(data) {

    this.auth.obtenerId(this.registerForm.value, data).subscribe(data2 => {
      this.getInfoFarm(data2)
    })
  }
  getInfoFarm(data2) {
    this.auth.getInfoFarm(data2).subscribe(data3 => {
      localStorage.setItem('account', JSON.stringify(data3[0]));
      this.router.navigateByUrl('/');
      
    }) 
  }

  get f() { return this.registerForm.controls; }
  
  openSnackBar(message) {
    this._snackBar.open(message, 'De acuerdo', {
      duration: 3000,
      verticalPosition: "top",
      horizontalPosition: "right"
    });
  }


}

