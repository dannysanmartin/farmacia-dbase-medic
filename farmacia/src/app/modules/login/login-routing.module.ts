import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFarmaciaComponent } from './login-farmacia/login-farmacia.component';


const routes: Routes = [
  {path: 'loginFarmacia',component: LoginFarmaciaComponent}
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
