import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class HeaderButtonsService {

  isDisabledButtons: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  disable$ = this.isDisabledButtons.asObservable();

  constructor() { }

  setDisable(disable: boolean) {
    this.isDisabledButtons.next(disable);
  }
}
