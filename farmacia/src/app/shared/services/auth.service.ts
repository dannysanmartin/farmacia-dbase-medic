import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  httpOptions: any = {};
  private url = environment.apiURL;
  private url1 = environment.apiURL1;
  private urlP = 'http://apiuser.azurewebsites.net/api/usuarios';

  constructor(private httpClient: HttpClient, private router: Router, private _snackBar: MatSnackBar) { }


  getUsuarios(): Observable<any> {

    return this.httpClient.get<any>(`${this.urlP}`);

  }

  isAuthenticated(): Boolean {
    return localStorage.getItem('account') != null ? true : false;
  }

  getTokenUser(data: any): Observable<any> {
    return this.httpClient.post<any>(`${this.url}` + 'medico/api-token-auth/', data).pipe(
      map(
        (token: any) => {

          this.httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': 'Token ' + token.token
            })
          }

        }
      )
    )
  }
  obtenerId(correo: any, data: any): Observable<any> {
    this.httpOptions.params = new HttpParams().set('username', correo.username);
    return this.httpClient.get<any>(`${this.url}` + 'medico/obtenerId/', this.httpOptions);
  }

  getInfoFarm(data: any): Observable<any> {
    return this.httpClient.get<any>(`${this.url}farmacia/getData?model=UserFarmacia&params=user_id=${data.jsonPk}`);
  }

  getTipoCatalogos(): Observable<any> {

    return this.httpClient.get<any>(`${this.url1}catalogo/listar/`);
  }
  createTipoCatalogo(data: any): Observable<any> {
    console.log('Data para node', data)
    return this.httpClient.post<any>(`${this.url1}catalogo/crear/`, data);
    
  }

  updateTipoCatalogo(data: any, id:any): Observable<any> {
    console.log('idddd',id);
    return this.httpClient.put<any>(`${this.url}` + 'farmacia/UpdateTipoCatalogos/'+id+'/', data);
    
  }


  logout() {
    localStorage.removeItem('account');
    localStorage.removeItem('token');
    this.router.navigateByUrl('loginFarmacia');

  }
}
