import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
@Component({
 selector: 'app-sidebar',
 templateUrl: './sidebar.component.html',
 styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
 @Output() changeHeader = new EventEmitter();
 nombre = 'inicio';
 userData: any;

 constructor(
   
 ) { }

 ngOnInit() {
   this.userData = JSON.parse(localStorage.getItem('account'));
  
 }
 cargarHeader(nombre: string) {
   this.nombre = nombre;
   this.changeHeader.emit(nombre);
 }
}