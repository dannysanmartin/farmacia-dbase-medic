import { Component, OnInit, Input, HostBinding } from '@angular/core';

import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../shared/services/auth.service';
import { HeaderButtonsService } from '../../services/header-buttons.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() header: string;
  @Input() data = {};
  userData: any;
  prueba: "prueba";
  photoProfile: any;
  isDisableButtons$: Observable<boolean>;
  disable: boolean = true;

  constructor(
    public authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private headerButtonsService: HeaderButtonsService

  ) { }

  ngOnInit() {
    this.getHeader();
    this.isDisableButtons$ = this.headerButtonsService.disable$;
    this.isDisableButtons$.subscribe(result => {
      this.disable = result;
    });
    this.userData = JSON.parse(localStorage.getItem('account'));

    if (this.userData.fotoPerfil === null || this.userData.fotoPerfil === undefined) {
      this.photoProfile = this.userData.priNombre[0].toUpperCase() + ' ' + this.userData.priApellido[0].toUpperCase();
    }
  }
  logout() {

    this.authService.logout();


  }
  getHeader() {
    switch (this.header) {
      case 'inicio':
        return `url('/assets/images/header/ImageB.png')`;
      case 'inventario':
        return `url('/assets/images/header/ImageC.png')`;
      case 'catalogos':
        return `url('/assets/images/header/ImageC.png')`;
      case 'perfil':
        return `url('/assets/images/header/ImageA.png')`;
    }
  }

}
